<?php

namespace Drupal\murz\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\EntityReferenceAutocompleteWidget;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\EntityOwnerInterface;
use Symfony\Component\Validator\ConstraintViolationInterface;

/**
 * Plugin implementation of the 'entity_reference_autocomplete' widget.
 *
 * @FieldWidget(
 *   id = "murz_reference_autocomplete_widget",
 *   label = @Translation("MurzReferenceAutocompleteWidget"),
 *   description = @Translation("MurzReferenceAutocompleteWidget"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class MurzReferenceAutocompleteWidget extends EntityReferenceAutocompleteWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    $field_name = $this->fieldDefinition->getName();
    $parents = $form['#parents'];
    $id_prefix = implode('-', array_merge($parents, [$field_name]));
    $wrapper_id = Html::getUniqueId($id_prefix . '-wrapper22');

    $form_input = $form_state->getUserInput();
    $field_input = NestedArray::getValue($form_state->getUserInput(), [...$parents, $field_name]);

    $form['#process'][] = [$this, 'formAfterProcess'];

    $element['#prefix'] = '<div id="' . $wrapper_id . '">';
    $element['#suffix'] = '</div>';

    $element['selector_type'] = [
      '#type' => 'radios',
      '#title' => t('Select by'),
      '#options' => [
        'title' => t('Title'),
        'alias' => t('URL Alias'),
      ],
      '#default_value' => 'title',
      '#weight' => '-1',
      '#ajax' => [
        "callback" => [$this, 'ajaxUpdate'],
        "event" => "change",
        "wrapper" => $wrapper_id,
        'effect' => 'fade',
      ],
    ];
    // $element['target_id']['#description'] = "asdas";


    // $element['target_id']['link_type'] = [
    //   '#markup' => 'aAAAA',
    // ];
    $autocomplete_type = $field_input[0]['selector_type'];

    $tag_id = $form_state->getValue('field_articles_tag')[0]['target_id'] ?? NULL;

    $element['target_id']['#description'] = "Autocomplete by: $autocomplete_type for tag id $tag_id - rand " . rand(100,200);
    $element['target_id']['#selection_settings']['view']['arguments'][0] = $tag_id;

    return $element;
  }

  public function ajaxUpdate($form, FormStateInterface $form_state) {
    $trigger = $form_state->getTriggeringElement();
    // Go one level up in the form, to the widgets container.
    $element = NestedArray::getValue($form, array_slice($trigger['#array_parents'], 0, -2));
    // $element['target_id']['#description'] = "Type: " . rand(100,200);
    return $element;
  }

  public function formAfterProcess($form, FormStateInterface $form_state) {

    $form['field_articles_tag']['widget']['#ajax']['wrapper'] = $form['#id'];
    return $form;
  }


}
