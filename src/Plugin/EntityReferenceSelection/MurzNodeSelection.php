<?php

namespace Drupal\murz\Plugin\EntityReferenceSelection;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\node\Plugin\EntityReferenceSelection\NodeSelection;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an entity reference selection for topics.
 *
 * @EntityReferenceSelection(
 *   id = "murz_node_selection",
 *   label = @Translation("Murz Node Selection"),
 *   entity_types = {"node"},
 *   group = "murz_node_selection",
 *   weight = 0
 * )
 */
class MurzNodeSelection extends NodeSelection implements ContainerFactoryPluginInterface {

  /**
   * Request.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Applications helper service.
   *
   * @var \Drupal\tvh_application\ApplicationsHelperInterface
   */
  // protected ApplicationsHelperInterface $applicationsHelper;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->requestStack = $container->get('request_stack');
    // $instance->applicationsHelper = $container->get('tvh_application.applications_helper');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'filter_application' => TRUE,
      'published_only' => TRUE,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  protected function buildEntityQuery($match = NULL, $match_operator = 'CONTAINS') {
    $query = parent::buildEntityQuery($match, $match_operator);
    // $configuration = $this->getConfiguration();

    // if ($configuration['filter_application']) {
    //   $applications = $this->applicationsHelper->getApplicationsFromRequest($this->requestStack->getCurrentRequest());
    //   if (!empty($applications)) {
    //     $query->condition('field_application', $applications, 'IN');
    //   }
    //   else {
    //     // Should force returning the empty result.
    //     // @todo Add proper applications handling for nested block.
    //     // $query->condition('field_application', -1);
    //   }
    // }

    // if ($configuration['published_only']) {
    //   $query->condition('status', NodeInterface::PUBLISHED);
    // }

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $configuration = $this->getConfiguration();

    $form['filter_application'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Filter by application'),
      '#default_value' => $configuration['filter_application'],
      '#description' => $this->t('Show matching applications results only.'),
    ];
    $form['published_only'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Published only'),
      '#default_value' => $configuration['published_only'],
      '#description' => $this->t('Show only published content in results.'),
    ];

    return $form;
  }

}
